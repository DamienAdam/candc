<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'mysql' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4[K>$:fRV12C.2EzilD!l_]PwqX&]ZcD$)xSRC;>I<hpvry%OXjw<f{Caw%SQi(i' );
define( 'SECURE_AUTH_KEY',  'qLk-TJ.S_2k2l/Kct)t[|cQ6QUn j-zv~=^yomA@$_}J<6>IYt63eLQhN-~j1?--' );
define( 'LOGGED_IN_KEY',    'y2Uquj8r 6;t$z WhA;~IKY|nq[L[].)_@<!J~R=`S^Z2w3~krhgBMD(2w`51 d^' );
define( 'NONCE_KEY',        'h(TEy&lj9U1OSF)<rgQRn!c7[GI$U8~HCu -2.Ca,QU__s5+DbazG`Ly 8+M~?>>' );
define( 'AUTH_SALT',        'zGT@:nc&+<2mo#|74N$Q~69{tv8*rzzRdc=jlIRw,5^9CF0Zp}:6XJQ!6U*Deb&L' );
define( 'SECURE_AUTH_SALT', '7Z1db=@I}-OG%isn>L5B4fqG-Z/(uVG/;O$+.6X_D&]u*8!&g}J#Eq}EcBvV;rgO' );
define( 'LOGGED_IN_SALT',   'jLKedH bjP(Cx^._ka (4+jwFf@+gLEB+_O-#D|Bz(5h=oeo?@~=7|E%0!Co${6)' );
define( 'NONCE_SALT',       'yDG*XzjV{xXuE+/`1Cv,!w#f8&Bcu(L[7N+ir82_BH]Y<966k(_ge~JvV}p](*V1' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
